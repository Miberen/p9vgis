﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using UnitySpeechToText.Utilities;
using VGIS;
using VRTK;

namespace UnitySpeechToText.Widgets
{
    /// <summary>
    /// Widget that handles the side-by-side comparison of different speech-to-text services.
    /// </summary>
    public class VGIS_SpeechCommands : MonoBehaviour
    {
        private static bool _commandAcquired;
        private static string compoundString;
        public GameObject cuberino;
        public Text feedBack;
        public VRTK_ControllerEvents controller = null;
        /// <summary>
        /// Store for ResponsesTimeoutInSeconds property
        /// </summary>
        [SerializeField]
        float m_ResponsesTimeoutInSeconds = 8f;
        /// <summary>
        /// Store for SpeechToTextServiceWidgets property
        /// </summary>
        [SerializeField]
        VGIS_SpeechToText[] m_SpeechToTextServiceWidgets;
        /// <summary>
        /// Whether the application is currently in a speech-to-text session
        /// </summary>
        bool m_IsCurrentlyInSpeechToTextSession;
        /// <summary>
        /// Whether the application is currently recording audio
        /// </summary>
        bool m_IsRecording;
        /// <summary>
        /// Set of speech-to-text service widgets that are still waiting on a final response
        /// </summary>
        HashSet<VGIS_SpeechToText> m_WaitingSpeechToTextServiceWidgets = new HashSet<VGIS_SpeechToText>();
        /// <summary>
        /// Number of seconds to wait for all responses after recording
        /// </summary>
        public float ResponsesTimeoutInSeconds { set { m_ResponsesTimeoutInSeconds = value; } }

        /// <summary>
        /// Array of speech-to-text service widgets 
        /// </summary>
        public VGIS_SpeechToText[] SpeechToTextServiceWidgets
        {
            set
            {
                m_SpeechToTextServiceWidgets = value;
                RegisterSpeechToTextServiceWidgetsCallbacks();
            }
        }
        /// <summary>
        /// Voice commands available in the system
        /// </summary>
        public static readonly List<string> voiceCommands = new List<string> {"change color", "rotate", "move", "scale", "reset", "cancel"};

        /// <summary>
        /// Initialization function called on the frame when the script is enabled just before any of the Update
        /// methods is called the first time.
        /// </summary>
        void Start()
        {
            RegisterSpeechToTextServiceWidgetsCallbacks();

            controller.TouchpadLeftPressed += DoTouchpadLeftPressed;
            controller.TouchpadLeftReleased += DoTouchpadLeftReleased;
        }

        private void DoTouchpadLeftPressed(object sender, ControllerInteractionEventArgs e)
        {
            //Debug.Log("RecordButtonPressed");
            OnRecordButtonClicked();
        }

        private void DoTouchpadLeftReleased(object sender, ControllerInteractionEventArgs e)
        {
            //Debug.Log("RecordButtonReleased");
            OnRecordButtonClicked();
        }
        void Update()
        {

            if (VGIS_Player.Instance.RightHand._currentlyPointing != null)
            {
                cuberino = VGIS_Player.Instance.RightHand._currentlyPointing.gameObject;
            }
        }

        /// <summary>
        /// Function that is called when the MonoBehaviour will be destroyed.
        /// </summary>
        void OnDestroy()
        {
            UnregisterSpeechToTextServiceWidgetsCallbacks();
        }


        /// <summary>
        /// Registers callbacks with each SpeechToTextServiceWidget.
        /// </summary>
        void RegisterSpeechToTextServiceWidgetsCallbacks()
        {
            if (m_SpeechToTextServiceWidgets != null)
            {
                //SmartLogger.Log(DebugFlags.SpeechToTextWidgets, "register service widgets callbacks");
                foreach (var serviceWidget in m_SpeechToTextServiceWidgets)
                {
                    //SmartLogger.Log(DebugFlags.SpeechToTextWidgets, "register service widget callbacks");
                    serviceWidget.RegisterOnRecordingTimeout(OnRecordTimeout);
                    serviceWidget.RegisterOnReceivedLastResponse(OnSpeechToTextReceivedLastResponse);
                }
            }
        }

        /// <summary>
        /// Unregisters callbacks with each SpeechToTextServiceWidget.
        /// </summary>
        void UnregisterSpeechToTextServiceWidgetsCallbacks()
        {
            if (m_SpeechToTextServiceWidgets != null)
            {
                //SmartLogger.Log(DebugFlags.SpeechToTextWidgets, "unregister service widgets callbacks");
                foreach (var serviceWidget in m_SpeechToTextServiceWidgets)
                {
                    //SmartLogger.Log(DebugFlags.SpeechToTextWidgets, "unregister service widget callbacks");
                    serviceWidget.UnregisterOnRecordingTimeout(OnRecordTimeout);
                    serviceWidget.UnregisterOnReceivedLastResponse(OnSpeechToTextReceivedLastResponse);
                }
            }
        }

        /// <summary>
        /// Function that is called when the record button is clicked.
        /// </summary>
        public void OnRecordButtonClicked()
        {
            if (m_IsRecording)
            {
                StopRecording();
                feedBack.color = Color.black;
            }
            else
            {
                _commandAcquired = false;
                StartRecording();
                feedBack.color = Color.red;
            }
        }

        /// <summary>
        /// Function that is called when audio recording times out.
        /// </summary>
        void OnRecordTimeout()
        {
            StopRecording();
        }

        /// <summary>
        /// Function that is called when the given SpeechToTextServiceWidget has gotten its last response. If there are no waiting
        /// SpeechToTextServiceWidgets left, then this function will wrap-up the current comparison session.
        /// </summary>
        /// <param name="serviceWidget">The speech-to-text service widget that received a last response</param>
        void OnSpeechToTextReceivedLastResponse(VGIS_SpeechToText serviceWidget)
        {
            //SmartLogger.Log(DebugFlags.SpeechToTextWidgets, "Response from " + serviceWidget.SpeechToTextServiceString());
            m_WaitingSpeechToTextServiceWidgets.Remove(serviceWidget);
            if (m_WaitingSpeechToTextServiceWidgets.Count == 0)
            {
                //SmartLogger.Log(DebugFlags.SpeechToTextWidgets, "Responses from everyone");
                FinishComparisonSession();
            }
            VoiceCommand(compoundString, cuberino);
        }

        /// <summary>
        /// Starts recording audio for each speech-to-text service widget if not already recording.
        /// </summary>
        void StartRecording()
        {
            if (!m_IsRecording)
            {
                //SmartLogger.Log(DebugFlags.SpeechToTextWidgets, "Start comparison recording");
                m_IsCurrentlyInSpeechToTextSession = true;
                m_IsRecording = true;
                m_WaitingSpeechToTextServiceWidgets.Clear();
                foreach (var serviceWidget in m_SpeechToTextServiceWidgets)
                {
                    //SmartLogger.Log(DebugFlags.SpeechToTextWidgets, "tell service widget to start recording");
                    serviceWidget.StartRecording();
                    m_WaitingSpeechToTextServiceWidgets.Add(serviceWidget);
                }
            }
        }

        /// <summary>
        /// Stops recording audio for each speech-to-text service widget if already recording. Also schedules a wrap-up of the
        /// current comparison session to happen after the responses timeout.
        /// </summary>
        void StopRecording()
        {
            if (m_IsRecording)
            {
                m_IsRecording = false;

                // Disable all UI interaction until all responses have been received or after the specified timeout.
                Invoke("FinishComparisonSession", m_ResponsesTimeoutInSeconds);

                foreach (var serviceWidget in m_SpeechToTextServiceWidgets)
                {
                    serviceWidget.StopRecording();
                }
            }
        }

        /// <summary>
        /// Wraps up the current speech-to-text comparison session by enabling all UI interaction.
        /// </summary>
        void FinishComparisonSession()
        {
            // If this function is called before the timeout, cancel all invokes so that it is not called again upon timeout.
            CancelInvoke();

            if (m_IsCurrentlyInSpeechToTextSession)
            {
                m_IsCurrentlyInSpeechToTextSession = false;
            }
        }

        public static void AddToCompoundString(string results)
        {
            if(!_commandAcquired)
            compoundString += " " + results;
        }

        private void VoiceCommand(string command, GameObject obj)
        {
            //Debug.Log(command);
            if (_commandAcquired)
                return;
            string detectedCommand = "No Command";
            foreach (var str in voiceCommands)
            {
                if (command.Contains(str))
                    detectedCommand = str;
            }
            string[] words = command.ToLower().Split(' ');

            for (int i = 0; i < words.Length; i++)
            {
                words[i] = words[i].Replace(" ", string.Empty);
            }
            
            switch (detectedCommand)
            {
                case "rotate":
                   
                    int p = 0;
                    string axis = "Not Found";
                     foreach (string word in words)
                    {
                        if(axis != "Not Found" && p != 0) break;

                        if (p == 0)
                            int.TryParse(word, out p);

                        if (word == "set" || word == "z" || word == "c" || word == "see"|| word == "sea")
                            axis = "z";
                        else if (word == "y" || word == "why")
                            axis = "y";
                        else if (word == "x")
                            axis = "x";
                    }
                    if (p != 0 && axis == "Not Found")
                    {
                        feedBack.text = "I got that you want to rotate by " + p + " degrees, but I didn't catch which axis?";
                        return;
                    }
                    if (p == 0 && axis != "Not Found")
                    {
                        feedBack.text = "I got that you want to rotate around the " + axis +"-axis, but I didn't catch how many degrees?";
                        return;
                    }
                    if (p == 0 || axis == "Not Found")
                    {
                        feedBack.text = "I got that you want to rotate, but I didn't catch how many degrees or which axis?";
                        return;
                    }
                    _commandAcquired = true;

                    if(axis == "z")
                        obj.transform.Rotate(0,0,p);
                    else if(axis == "y")
                        obj.transform.Rotate(0, p, 0);
                    else if(axis == "x")
                        obj.transform.Rotate(p,0,0);
                    compoundString = "";
                    feedBack.text = "I rotated the object " + p + " degrees around the " + axis + "-axis";
                    break;

                case " change color":
                case "change color ":
                case "change color":
                    Color color = Color.clear;
                    foreach (string word in words)
                    {
                       
                        switch (word)
                        {
                            case "black":
                                color = Color.black;
                                break;
                            case "gray":
                                color = Color.gray;
                                break;
                            case "grey":
                                color = Color.grey;
                                break;
                            case "green":
                                color = Color.green;
                                break;
                            case "blue":
                                color = Color.blue;
                                break;
                            case "cyan":
                                color = Color.cyan;
                                break;
                            case "magenta":
                                color = Color.magenta;
                                break;
                            case "red":
                                color = Color.red;
                                break;
                            case "white":
                                color = Color.white;
                                break;
                            case "yellow":
                                color = Color.yellow;
                                break;
                        }
                        if (color != Color.clear) break;
                    }
                    if (color == Color.clear)
                    {
                        feedBack.text = "I got that you want to change color, but which color do you want to change to?";
                        return;
                    }
                    cuberino.GetComponent<Renderer>().sharedMaterial.SetColor("_Color", color);
                    _commandAcquired = true;
                    feedBack.text = "I changed the color of the object to " + color;
                    compoundString = "";
                    break;
                case "move":
                    feedBack.text = "Move is not yet avaiable, please do another command";
                    _commandAcquired = true;
                    compoundString = "";
                    break;
                case "scale":
                    float s = 0;
                    foreach (string word in words)
                    {
                        if (s != 0) break;
                        if (s == 0) float.TryParse(word, out s);
                    }
                    if (s == 0)
                    {
                        feedBack.text = "I got that you want to scale, but by how much? Please give me any decimal number and I will scale it for you";
                        return;
                    }
                    cuberino.transform.localScale = new Vector3(s,s,s);
                    _commandAcquired = true;
                    feedBack.text = "I scaled the object to " + s + " times its original size";
                    compoundString = "";
                    break;
                case "reset":
                    if (words.Any(word => word == "rotation"))
                    {
                        cuberino.transform.eulerAngles = new Vector3(0, 0, 0);
                        _commandAcquired = true;
                        compoundString = "";
                        feedBack.text = "I reset the rotation of the object. Is there something else you want me to do?";
                    }
                    else
                    {
                        feedBack.text = "I got that you want to reset, but which parameter did you want to reset? Rotation, Color or Scale?";
                    }
                    break;
                case "cancel":
                    compoundString = "";
                    _commandAcquired = true;
                    feedBack.text = "I canceled all previous input, please give me a new command";
                    break;
                default:
                    feedBack.text = "I didn't catch if you want to rotate, scale or change color? or perhaps you want to reset a paremeter or cancel all current input?";
                    feedBack.text += "\nHere is what i have to so far: " + compoundString;
                    break;
            }           
        }
    }
}
