﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NewtonVR;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Valve.VR;
using VRTK;
using VGIS;

namespace VGIS
{
    public class VGIS_Interaction : MonoBehaviour
    {

        private delegate void UpdateFunction();
        private UpdateFunction updateFunction;
        private delegate void LateUpdateFunction();
        private LateUpdateFunction lateUpdateFunction;
        private delegate void FixedUpdateFunction();
        private FixedUpdateFunction fixedUpdateFunction;



        #region LaserVars

        [Header("Laser Settings", order = 1)]
        public Color LineColor = Color.black;
        public Color LineColorMenu = Color.cyan;
        public float LineWidth = 0.005f;
        public RaycastHit Hit;
        public GameObject StartPoint;
        public GameObject InteractionPoint;
        private LineRenderer _line;
        public VGIS_Interactable _currentlyPointing;

        #endregion

        #region UIVars

        #endregion

        #region TeleporterVars

        [Header("Teleporter Settings", order = 2)]

        #endregion

        #region ControlVars

        [Header("Control Settings", order = 3)]
        public bool touchPadIsWheel = false;

        public float previousPadValue = 0;
        public float deltaPadScroll = 0;
        public float scrollSensitivity = 1;

        #endregion

        #region HandVars

        [Header("Hand Settings", order = 4)]
        [Tooltip(
                                                  "If you want to use something other than the standard SteamVR Controller models place the Prefab here. Otherwise we use steamvr models."
                                              )]
        public GameObject CustomModel;

        [Tooltip(
             "If you're using a custom model or if you just want custom physical colliders, stick the prefab for them here."
         )]
        public GameObject CustomPhysicalColliders;

        public Color SelectionColor;
        public HandState CurrentHandState = HandState.Uninitialized;
        public InteractionStyles ControllerStyle = InteractionStyles.GdToInt;
        public InteractionStyles LaserStyle = InteractionStyles.GtToInt;
        public Rigidbody Rigidbody;
        public VGIS_Interactable CurrentlyInteracting;
        public bool SelectEntireObject = false;
        public bool UseHoverHightlight = true;
        public VGIS_Interactable CurrentlySelecting;
        public bool IsUsingMenu;
        private SteamVR_Controller.Device _controller;
        private VisibilityLevel _currentVisibility = VisibilityLevel.Visible;
        private bool _renderModelInitialized = false;
        public float SnapDistanceMultiplier = 0.6f;
        public Dictionary<VGIS_Interactable, Dictionary<Collider, float>> CurrentlyHoveringOver;
        private VGIS_PhysicalController _physicalController;
        private Collider[] _ghostColliders;
        private MeshRenderer[] _ghostRenderers;
        private int _deviceIndex = -1;

        private int _estimationSampleIndex;
        private Vector3[] _lastPositions;
        private Quaternion[] _lastRotations;
        private float[] _lastDeltas;
        private const int _estimationSamples = 5;
        private const int _rotationEstimationSamples = 10;

        public bool IsHovering
        {
            get { return CurrentlyHoveringOver.Any(kvp => kvp.Value.Count > 0); }
        }

        public bool IsInteracting
        {
            get { return CurrentlyInteracting != null; }
        }

        private VRTK_ControllerEvents controllerEvents;
        private VRTK_ControllerActions controllerActions;

        #endregion

        #region GUIPointerVars

        #endregion

        protected virtual void Awake()
        {
            #region HandRelated

            controllerEvents = transform.GetComponent<VRTK_ControllerEvents>();
            controllerActions = transform.GetComponent<VRTK_ControllerActions>();

            CurrentlyHoveringOver = new Dictionary<VGIS_Interactable, Dictionary<Collider, float>>();

            _lastPositions = new Vector3[_estimationSamples];
            _lastRotations = new Quaternion[_rotationEstimationSamples];
            _lastDeltas = new float[_estimationSamples];
            _estimationSampleIndex = 0;


            SteamVR_Utils.Event.Listen("render_model_loaded", RenderModelLoaded);
            SteamVR_Utils.Event.Listen("new_poses_applied", OnNewPosesApplied);


            #endregion

        }

        private void OnDestroy()
        {
            SteamVR_Utils.Event.Remove("render_model_loaded", RenderModelLoaded);
            SteamVR_Utils.Event.Remove("new_poses_applied", OnNewPosesApplied);
        }

        private void RenderModelLoaded(params object[] args)
        {
            SteamVR_RenderModel renderModel = (SteamVR_RenderModel)args[0];
            bool success = (bool)args[1];

            if ((int)renderModel.index == _deviceIndex)
                _renderModelInitialized = true;
        }

        private void OnNewPosesApplied(params object[] args)
        {
            if (_controller == null)
                return;

            if (CurrentlyInteracting != null)
            {
                CurrentlyInteracting.OnNewPosesApplied();
            }
        }

        protected virtual void OnTriggerEnter(Collider collider)
        {
            VGIS_Interactable interactable = VGIS_Interactables.GetInteractable(collider);
            if (interactable == null || interactable.enabled == false) return;

            if (CurrentlyHoveringOver.ContainsKey(interactable) == false)
                CurrentlyHoveringOver[interactable] = new Dictionary<Collider, float>();

            if (CurrentlyHoveringOver[interactable].ContainsKey(collider) == false)
                CurrentlyHoveringOver[interactable][collider] = Time.time;
        }

        protected virtual void OnTriggerStay(Collider collider)
        {
            VGIS_Interactable interactable = VGIS_Interactables.GetInteractable(collider);
            if (interactable == null || interactable.enabled == false)
                return;

            if (CurrentlyHoveringOver.ContainsKey(interactable) == false)
                CurrentlyHoveringOver[interactable] = new Dictionary<Collider, float>();

            if (CurrentlyHoveringOver[interactable].ContainsKey(collider) == false)
                CurrentlyHoveringOver[interactable][collider] = Time.time;

        }

        protected virtual void OnTriggerExit(Collider collider)
        {
            VGIS_Interactable interactable = VGIS_Interactables.GetInteractable(collider);
            if (interactable == null)
                return;

            if (CurrentlyHoveringOver.ContainsKey(interactable) == true)
            {
                if (CurrentlyHoveringOver[interactable].ContainsKey(collider) == true)
                {
                    CurrentlyHoveringOver[interactable].Remove(collider);
                    if (CurrentlyHoveringOver[interactable].Count == 0)
                    {
                        CurrentlyHoveringOver.Remove(interactable);
                    }
                }
            }
        }

        protected virtual void OnEnable()
        {
            if (this.gameObject.activeInHierarchy)
                StartCoroutine(DoInitialize());
        }

        // Use this for initialization
        void Start()
        {
            SetupControllerEvents();

            #region LaserRelated

            StartPoint = new GameObject("LaserStart " + transform.name);
            StartPoint.transform.SetParent(this.transform.GetComponentInChildren<Transform>());
            StartPoint.transform.rotation = this.transform.rotation * Quaternion.Euler(57, 0, 0);
            StartPoint.transform.localPosition = new Vector3(0, -0.0669f, 0.0345f);

            InteractionPoint = new GameObject("LaserInteract " + transform.name);
            InteractionPoint.transform.SetParent(StartPoint.transform);
            InteractionPoint.transform.localPosition = new Vector3(0, 0, 0);
            InteractionPoint.transform.rotation = StartPoint.transform.rotation;

            if (_line == null)
            {
                _line = this.gameObject.AddComponent<LineRenderer>();
                _line.enabled = false;
            }
            if (_line.sharedMaterial == null)
            {
                _line.material = new Material(Shader.Find("Unlit/Color"));
                _line.material.SetColor("_Color", LineColor);
                _line.SetColors(LineColor, LineColor);
            }
            _line.useWorldSpace = true;

            #endregion

            lateUpdateFunction += LateUpdateHandHoverHightlight;
        }

        // Update is called once per frame
        void Update()
        {
            if (updateFunction != null)
            {
                updateFunction();
            }
            if (touchPadIsWheel && controllerEvents.touchpadTouched && previousPadValue != 0f)
            {
                deltaPadScroll = ((controllerEvents.GetTouchpadAxis().y + 1 - previousPadValue + 1) - 2) * scrollSensitivity * 
                    Mathf.Min(Mathf.Max(Mathf.Pow(
                        Vector3.Distance(StartPoint.transform.localPosition, InteractionPoint.transform.localPosition)/2,
                        1.5f), 0.5f),50);
            }
            else
            {
                deltaPadScroll = 0;
            }
            previousPadValue = controllerEvents.GetTouchpadAxis().y;

        }

        void LateUpdate()
        {
            if (lateUpdateFunction != null)
            {
                lateUpdateFunction();
            }

        }

        void FixedUpdate()
        {
            if (fixedUpdateFunction != null)
            {
                fixedUpdateFunction();
            }
        }

        public Vector3 GetVelocityEstimation()
        {
            float delta = _lastDeltas.Sum();
            Vector3 distance = Vector3.zero;

            for (int index = 0; index < _lastPositions.Length - 1; index++)
            {
                Vector3 diff = _lastPositions[index + 1] - _lastPositions[index];
                distance += diff;
            }

            return distance / delta;
        }

        public Vector3 GetAngularVelocityEstimation()
        {
            float delta = _lastDeltas.Sum();
            float angleDegrees = 0.0f;
            Vector3 unitAxis = Vector3.zero;
            Quaternion rotation = Quaternion.identity;

            rotation = _lastRotations[_lastRotations.Length - 1] *
                       Quaternion.Inverse(_lastRotations[_lastRotations.Length - 2]);

            //Error: the incorrect rotation is sometimes returned
            rotation.ToAngleAxis(out angleDegrees, out unitAxis);
            return unitAxis * ((angleDegrees * Mathf.Deg2Rad) / delta);
        }

        public Vector3 GetPositionDelta()
        {
            int last = _estimationSampleIndex - 1;
            int secondToLast = _estimationSampleIndex - 2;

            if (last < 0)
                last += _estimationSamples;
            if (secondToLast < 0)
                secondToLast += _estimationSamples;

            return _lastPositions[last] - _lastPositions[secondToLast];
        }

        public Quaternion GetRotationDelta()
        {
            int last = _estimationSampleIndex - 1;
            int secondToLast = _estimationSampleIndex - 2;

            if (last < 0)
                last += _estimationSamples;
            if (secondToLast < 0)
                secondToLast += _estimationSamples;

            return _lastRotations[last] * Quaternion.Inverse(_lastRotations[secondToLast]);
        }

        public void ForceGhost()
        {
            SetVisibility(VisibilityLevel.Ghost);
            _physicalController.Off();
        }

        public bool ChangeState(HandState newState)
        {
            //Not needed?
            //if (_controller != null)
            //{
            //    CurrentHandState = HandState.Idle;
            //}

            bool value;
            switch (newState)
            {
                case HandState.Idle:
                    lateUpdateFunction += LateUpdateHandHoverHightlight;
                    CurrentHandState = newState;
                    SetVisibility(VisibilityLevel.Ghost);
                    value = true;
                    break;
                case HandState.LaserNonInt:
                    lateUpdateFunction -= LateUpdateHandHoverHightlight;
                    UpdateHoverHightlight(null);
                    CurrentHandState = newState;
                    SetVisibility(VisibilityLevel.Ghost);
                    value = true;
                    break;
                case HandState.LaserInt:
                    CurrentHandState = newState;
                    SetVisibility(VisibilityLevel.Ghost);
                    value = true;
                    break;
                case HandState.GdNonInt:
                    CurrentHandState = newState;
                    SetVisibility(VisibilityLevel.Visible);
                    value = true;
                    break;
                case HandState.GdInt:
                    lateUpdateFunction -= LateUpdateHandHoverHightlight;
                    UpdateHoverHightlight(null);
                    CurrentHandState = newState;
                    SetVisibility(VisibilityLevel.Ghost);
                    value = true;
                    break;
                case HandState.GtNonInt:
                    CurrentHandState = newState;
                    SetVisibility(VisibilityLevel.Visible);
                    value = true;
                    break;
                case HandState.GtInt:
                    lateUpdateFunction -= LateUpdateHandHoverHightlight;
                    UpdateHoverHightlight(null);
                    CurrentHandState = newState;
                    SetVisibility(VisibilityLevel.Ghost);
                    value = true;
                    break;
                case HandState.MenuInt:
                    CurrentHandState = newState;
                    SetVisibility(VisibilityLevel.Ghost);
                    value = true;
                    break;
                default:
                    Debug.Log("State switching error");
                    value = false;
                    break;

            }
            if (VGIS_Player.Instance.MakeControllerInvisibleOnInteraction)
            {
                if (IsInteracting == true)
                {
                    SetVisibility(VisibilityLevel.Invisible);
                }
                else if (IsInteracting == false)
                {
                    SetVisibility(VisibilityLevel.Ghost);
                }
            }
            return value;
        }

        public string GetDeviceName()
        {
            if (CustomModel != null)
            {
                return "Custom";
            }
            else
            {
                return this.GetComponentInChildren<SteamVR_RenderModel>().renderModelName;
            }
        }

        private void SetVisibility(VisibilityLevel visibility)
        {
            if (_currentVisibility != visibility)
            {
                if (visibility == VisibilityLevel.Invisible)
                {
                    if (_physicalController != null)
                    {
                        _physicalController.Off();
                    }

                    for (int index = 0; index < _ghostRenderers.Length; index++)
                    {
                        _ghostRenderers[index].enabled = false;
                    }

                    for (int index = 0; index < _ghostColliders.Length; index++)
                    {
                        _ghostColliders[index].enabled = true;
                    }
                }

                if (visibility == VisibilityLevel.Ghost)
                {
                    if (_physicalController != null)
                    {
                        _physicalController.Off();
                    }

                    foreach (MeshRenderer t in _ghostRenderers)
                    {
                        t.enabled = true;
                    }

                    foreach (Collider t in _ghostColliders)
                    {
                        t.enabled = true;
                    }
                }

                if (visibility == VisibilityLevel.Visible)
                {
                    if (_physicalController != null)
                    {
                        _physicalController.On();
                    }

                    foreach (MeshRenderer t in _ghostRenderers)
                    {
                        t.enabled = false;
                    }

                    for (int index = 0; index < _ghostColliders.Length; index++)
                    {
                        _ghostColliders[index].enabled = false;
                    }
                }
            }

            _currentVisibility = visibility;
        }

        private IEnumerator DoInitialize()
        {
            do
            {
                yield return null; //wait for render model to be initialized
            } while (_renderModelInitialized == false && CustomModel == null);

            Rigidbody = this.GetComponent<Rigidbody>();
            if (Rigidbody == null)
                Rigidbody = this.gameObject.AddComponent<Rigidbody>();
            Rigidbody.isKinematic = true;
            Rigidbody.maxAngularVelocity = float.MaxValue;
            Rigidbody.useGravity = false;

            Collider[] Colliders = null;

            if (CustomModel == null)
            {
                string controllerModel = GetDeviceName();
                SteamVR_RenderModel renderModel = this.GetComponentInChildren<SteamVR_RenderModel>();

                switch (controllerModel)
                {
                    case "vr_controller_05_wireless_b":
                        Transform dk1Trackhat = renderModel.transform.Find("trackhat");
                        if (dk1Trackhat == null)
                        {
                            // Dk1 controller model has trackhat
                        }
                        else
                        {
                            dk1Trackhat.gameObject.SetActive(true);
                        }

                        SphereCollider dk1TrackhatCollider = dk1Trackhat.gameObject.GetComponent<SphereCollider>();
                        if (dk1TrackhatCollider == null)
                        {
                            dk1TrackhatCollider = dk1Trackhat.gameObject.AddComponent<SphereCollider>();
                            dk1TrackhatCollider.isTrigger = true;
                        }

                        Colliders = new Collider[] { dk1TrackhatCollider };
                        break;

                    case "vr_controller_vive_1_5":
                        Transform dk2Trackhat = renderModel.transform.FindChild("trackhat");
                        if (dk2Trackhat == null)
                        {
                            dk2Trackhat = new GameObject("trackhat").transform;
                            dk2Trackhat.gameObject.layer = this.gameObject.layer;
                            dk2Trackhat.parent = renderModel.transform;
                            dk2Trackhat.localPosition = new Vector3(0, -0.033f, 0.014f);
                            dk2Trackhat.localScale = Vector3.one * 0.1f;
                            dk2Trackhat.localEulerAngles = new Vector3(325, 0, 0);
                            dk2Trackhat.gameObject.SetActive(true);
                        }
                        else
                        {
                            dk2Trackhat.gameObject.SetActive(true);
                        }

                        Collider dk2TrackhatCollider = dk2Trackhat.gameObject.GetComponent<SphereCollider>();
                        if (dk2TrackhatCollider == null)
                        {
                            dk2TrackhatCollider = dk2Trackhat.gameObject.AddComponent<SphereCollider>();
                            dk2TrackhatCollider.isTrigger = true;
                        }

                        Colliders = new Collider[] { dk2TrackhatCollider };
                        break;

                    default:
                        Debug.LogError("Error. Unsupported device type: " + controllerModel);
                        break;
                }
            }
            else if (_renderModelInitialized == false)
            {
                _renderModelInitialized = true;
                GameObject CustomModelObject = GameObject.Instantiate(CustomModel);
                Colliders = CustomModelObject.GetComponentsInChildren<Collider>();
                //note: these should be trigger colliders

                CustomModelObject.transform.parent = this.transform;
                CustomModelObject.transform.localScale = Vector3.one;
                CustomModelObject.transform.localPosition = Vector3.zero;
                CustomModelObject.transform.localRotation = Quaternion.identity;
            }

            VGIS_Player.Instance.RegisterHand(this);

            if (VGIS_Player.Instance.PhysicalHands == true)
            {
                if (_physicalController != null)
                {
                    _physicalController.Kill();
                }

                _physicalController = this.gameObject.AddComponent<VGIS_PhysicalController>();
                _physicalController.Initialize(this, false);

                Color transparentcolor = Color.white;
                transparentcolor.a = (float)VisibilityLevel.Ghost / 100f;

                _ghostRenderers = this.GetComponentsInChildren<MeshRenderer>();
                for (int rendererIndex = 0; rendererIndex < _ghostRenderers.Length; rendererIndex++)
                {

                    NVRHelpers.SetTransparent(_ghostRenderers[rendererIndex].material, transparentcolor);
                }


                if (Colliders != null)
                {
                    _ghostColliders = Colliders;
                }

                _currentVisibility = VisibilityLevel.Ghost;
            }
            else
            {
                Color transparentcolor = Color.white;
                transparentcolor.a = (float)VisibilityLevel.Ghost / 100f;

                _ghostRenderers = this.GetComponentsInChildren<MeshRenderer>();
                for (int rendererIndex = 0; rendererIndex < _ghostRenderers.Length; rendererIndex++)
                {
                    NVRHelpers.SetTransparent(_ghostRenderers[rendererIndex].material, transparentcolor);
                }

                if (Colliders != null)
                {
                    _ghostColliders = Colliders;
                }

                _currentVisibility = VisibilityLevel.Ghost;
            }

            CurrentHandState = HandState.Idle;
        }

        public void DeregisterInteractable(VGIS_Interactable interactable)
        {
            if (CurrentlyInteracting == interactable)
                CurrentlyInteracting = null;

            if (CurrentlyHoveringOver != null)
                CurrentlyHoveringOver.Remove(interactable);
        }

        private void SetDeviceIndex(int index)
        {
            _deviceIndex = index;
            _controller = SteamVR_Controller.Input(index);
            StartCoroutine(DoInitialize());
        }

        private void UpdateHoverHightlight(VGIS_Interactable newItem)
        {
            if (UseHoverHightlight)
            {
                if (_currentlyPointing != null && _currentlyPointing == newItem && !newItem.IsHoverHightlighted/* && this != newItem.HoveringHand*/)
                //Same item to same item (used for overlap between hands)
                {
                    _currentlyPointing.StopHoverHightlight(this);
                    _currentlyPointing = newItem;
                    _currentlyPointing.StartHoverHightlight(this, SelectionColor);
                }
                else if (_currentlyPointing != null && newItem != null && _currentlyPointing != newItem)
                //From one item to another
                {
                    _currentlyPointing.StopHoverHightlight(this);
                    _currentlyPointing = newItem;
                    _currentlyPointing.StartHoverHightlight(this, SelectionColor);
                }
                else if (_currentlyPointing != null && newItem == null) //From something to nothing
                {
                    _currentlyPointing.StopHoverHightlight(this);
                    _currentlyPointing = null;
                }
                else if (newItem != null && _currentlyPointing == null) //From nothing to something
                {
                    _currentlyPointing = newItem;
                    _currentlyPointing.StartHoverHightlight(this, SelectionColor);
                }
            }
            else
            {
                if (_currentlyPointing == null) return;
                _currentlyPointing.StopHoverHightlight(this);
                _currentlyPointing = null;
            }
        }

        public virtual void BeginInteraction(VGIS_Interactable interactable)
        {
            if (interactable.CanAttach == true)
            {
                if (interactable.AttachedHand != null)
                {
                    interactable.AttachedHand.EndInteraction(interactable);
                }
                CurrentlyInteracting = interactable;
                CurrentlyInteracting.BeginInteraction(this);
            }
        }

        public virtual void EndInteraction(VGIS_Interactable item)
        {
            if (CurrentlyInteracting != null)
            {
                if (CurrentlyInteracting.GetType() == typeof(VGIS_InteractableItemSnapable))
                {
                    VGIS_InteractableItemSnapable snappy = (VGIS_InteractableItemSnapable)CurrentlyInteracting;
                    if (snappy.Assembly.GetDistanceToGhost(snappy) <
                        snappy.Renderer.bounds.size.magnitude * SnapDistanceMultiplier && item != snappy)
                    {
                        CurrentlyInteracting.EndInteraction();
                        CurrentlyInteracting = null;
                        CurrentlyHoveringOver.Remove(snappy);
                        snappy.BeginSnap(this);
                    }
                    else
                    {
                        CurrentlyInteracting.EndInteraction();
                        CurrentlyInteracting = null;
                    }

                    updateFunction -= UpdateCheckSnapDistance;
                    snappy.Assembly.DisableGhost(snappy);
                }
                else
                {
                    CurrentlyInteracting.EndInteraction();
                    CurrentlyInteracting = null;
                }
            }

            switch (CurrentHandState)
            {
                case HandState.LaserInt:
                    ChangeState(HandState.LaserNonInt);
                    break;
                case HandState.GtInt:
                case HandState.GdInt:
                    ChangeState(HandState.Idle);
                    break;
            }

            if (item != null && CurrentlyHoveringOver.ContainsKey(item) == true)
                CurrentlyHoveringOver.Remove(item);
        }

        private bool PickupInteractable()
        {
            VGIS_Interactable target = null;

            if (CurrentHandState == HandState.Idle)
            {

                float closestDistance = float.MaxValue;

                foreach (var hovering in CurrentlyHoveringOver)
                {
                    if (hovering.Key == null)
                        continue;

                    float distance = Vector3.Distance(this.transform.position, hovering.Key.transform.position);
                    if (!(distance < closestDistance)) continue;
                    closestDistance = distance;

                    target = SelectEntireObject
                        ? hovering.Key.transform.root.GetComponent<VGIS_Interactable>()
                        : hovering.Key;
                }

                if (target != null)
                {
                    if (target.transform.GetComponent<VGIS_InteractableItemSnapable>())
                    {
                        VGIS_InteractableItemSnapable tar = target.GetComponent<VGIS_InteractableItemSnapable>();

                        if (tar.IsSnapped())
                        {
                            tar.EndSnap();
                            BeginInteraction(tar);
                            tar.Assembly.ShowGhost(tar);
                            updateFunction += UpdateCheckSnapDistance;
                        }
                        else
                        {
                            BeginInteraction(tar);
                            tar.Assembly.ShowGhost(tar);
                            updateFunction += UpdateCheckSnapDistance;
                        }
                        return true;

                    }
                    if (target.transform.GetComponent<VGIS_InteractableItem>())
                    {
                        target = target.GetComponent<VGIS_InteractableItem>();
                        BeginInteraction(target);
                        return true;
                    }
                }
                else
                    return false;

            }
            if (CurrentHandState == HandState.LaserNonInt)
            {
                VGIS_Interactable target2 = null;

                if (Hit.transform)
                    target2 = SelectEntireObject
                        ? Hit.transform.GetComponent<VGIS_Interactable>()
                        : Hit.collider.gameObject.GetComponent<VGIS_Interactable>();

                if (target2 != null && target2.GetComponent<VGIS_InteractableItemSnapable>())
                {
                    VGIS_InteractableItemSnapable tar = target2.GetComponent<VGIS_InteractableItemSnapable>();
                    if (tar.IsSnapped())
                    {
                        tar.EndSnap();
                        ChangeState(HandState.LaserInt);
                        BeginInteraction(tar);
                        tar.Assembly.ShowGhost(tar);
                        updateFunction += UpdateCheckSnapDistance;
                    }
                    else
                    {
                        ChangeState(HandState.LaserInt);
                        BeginInteraction(tar);
                        tar.Assembly.ShowGhost(tar);
                        updateFunction += UpdateCheckSnapDistance;
                    }
                    return true;
                }
                if (target2 != null && target2.transform.GetComponent<VGIS_InteractableItem>())
                {
                    target = target2.GetComponent<VGIS_InteractableItem>();
                    ChangeState(HandState.LaserInt);
                    BeginInteraction(target);
                    return true;
                }
                else
                    return false;
            }
            else
                return false;

        }

        #region ButtonEvents

        private void SetupControllerEvents()
        {
            //controllerEvents.TriggerPressed += DoTriggerPressed;
            controllerEvents.TriggerReleased += DoTriggerReleased;

            //controllerEvents.TriggerTouchStart += DoTriggerTouchStart;
            //controllerEvents.TriggerTouchEnd += DoTriggerTouchEnd;

            //controllerEvents.TriggerHairlineStart += DoTriggerHairlineStart;
            //controllerEvents.TriggerHairlineEnd += DoTriggerHairlineEnd;

            controllerEvents.TriggerClicked += DoTriggerClicked;
            //controllerEvents.TriggerUnclicked += DoTriggerUnclicked;

            //controllerEvents.TriggerAxisChanged += DoTriggerAxisChanged;

            controllerEvents.ApplicationMenuPressed += DoApplicationMenuPressed;
            //controllerEvents.ApplicationMenuReleased += DoApplicationMenuReleased;

            controllerEvents.GripPressed += DoGripPressed;
            controllerEvents.GripReleased += DoGripReleased;

            controllerEvents.TouchpadPressed += DoTouchpadPressed;
            controllerEvents.TouchpadReleased += DoTouchpadReleased;
            controllerEvents.TouchpadUpPressed += DoTouchpadUpPressed;

            //controllerEvents.TouchpadTouchStart += DoTouchpadTouchStart;
            //controllerEvents.TouchpadTouchEnd += DoTouchpadTouchEnd;

            controllerEvents.TouchpadAxisChanged += DoTouchpadAxisChanged;
        }

        private void DoTriggerClicked(object sender, ControllerInteractionEventArgs e)
        {
            // code for text input
            if (EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.GetComponent<InputField>())
            {
                VGIS_TextInput.Instance.TargetInputField = EventSystem.current.currentSelectedGameObject.GetComponent<InputField>();
                SteamVR.instance.overlay.ShowKeyboard(0, 0, "Description", 256, "", true, 1L);
            }

            if (IsUsingMenu) return;
            switch (CurrentHandState)
            {
                case HandState.Idle:
                    VGIS_Interactable target = null;
                    float closestDistance = float.MaxValue;
                    foreach (var hovering in CurrentlyHoveringOver)
                    {
                        if (hovering.Key == null)
                            continue;

                        float distance = Vector3.Distance(this.transform.position, hovering.Key.transform.position);
                        if (!(distance < closestDistance)) continue;
                        closestDistance = distance;

                        target = SelectEntireObject
                            ? hovering.Key.transform.root.GetComponent<VGIS_Interactable>()
                            : hovering.Key;
                    }

                    if (target != null && target.GetComponent<VGIS_Interactable>() != null && target.IsSelected == false)
                    {
                        if (CurrentlySelecting != null)
                            CurrentlySelecting.EndSelect();
                        CurrentlySelecting = target;
                        CurrentlySelecting.BeginSelect(this, SelectionColor);
                    }
                    else if (CurrentlySelecting != null)
                    {
                        CurrentlySelecting.EndSelect();
                        CurrentlySelecting = null;
                    }
                    break;
                case HandState.LaserNonInt:
                    VGIS_Interactable target2 = null;

                    if (Hit.transform)
                        target2 = SelectEntireObject
                            ? Hit.transform.GetComponent<VGIS_Interactable>()
                            : Hit.collider.gameObject.GetComponent<VGIS_Interactable>();

                    if (Hit.transform != null && target2 != null && target2.IsSelected == false)
                    {
                        if (CurrentlySelecting != null)
                            CurrentlySelecting.EndSelect();

                        CurrentlySelecting = target2;
                        CurrentlySelecting.BeginSelect(this, SelectionColor);
                    }
                    else if (CurrentlySelecting != null)
                    {
                        CurrentlySelecting.EndSelect();
                        CurrentlySelecting = null;
                    }
                    break;
            }
        }

        private void DoTriggerReleased(object sender, ControllerInteractionEventArgs e)
        {

        }

        private void DoTouchpadPressed(object sender, ControllerInteractionEventArgs e)
        {
            if (e.touchpadAngle <= 45 || e.touchpadAngle > 315)
                return;
            else if (e.touchpadAngle > 45 && e.touchpadAngle <= 135)
                return;
            else if (e.touchpadAngle > 135 && e.touchpadAngle < 225)
                return;
            else
                return;
        }

        private void DoTouchpadUpPressed(object sender, ControllerInteractionEventArgs e)
        {
            switch (CurrentHandState)
            {
                case HandState.Idle:
                    ChangeState(HandState.LaserNonInt);
                    lateUpdateFunction += LateUpdateLaser;
                    _line.enabled = true;
                    break;
                case HandState.LaserNonInt:
                    ChangeState(HandState.Idle);
                    lateUpdateFunction -= LateUpdateLaser;
                    _line.enabled = false;
                    break;
                case HandState.LaserInt:
                    EndInteraction(null);
                    lateUpdateFunction -= LateUpdateLaser;
                    _line.enabled = false;
                    ChangeState(HandState.Idle);
                    break;
            }
        }

        private void DoTouchpadReleased(object sender, ControllerInteractionEventArgs e)
        {

        }

        private void DoGripPressed(object sender, ControllerInteractionEventArgs e)
        {
            switch (CurrentHandState)
            {
                case HandState.Idle:
                    if (PickupInteractable())
                    {
                        switch (ControllerStyle)
                        {
                            case InteractionStyles.GdToInt:
                                ChangeState(HandState.GdInt);
                                break;
                            case InteractionStyles.GtToInt:
                                ChangeState(HandState.GtInt);
                                break;
                        }
                    }
                    else
                    {
                        switch (ControllerStyle)
                        {
                            case InteractionStyles.GdToInt:
                                ChangeState(HandState.GdNonInt);
                                break;
                            case InteractionStyles.GtToInt:
                                ChangeState(HandState.GtNonInt);
                                break;
                        }
                    }
                    break;
                case HandState.GtInt:
                    EndInteraction(null);
                    break;
                case HandState.GtNonInt:
                    ChangeState(HandState.Idle);
                    break;
                case HandState.LaserNonInt:
                    PickupInteractable();
                    break;
                case HandState.LaserInt:
                    EndInteraction(null);
                    Hit = new RaycastHit(); // Bit of a hack.
                    break;
                default:
                    break;
            }
        }

        private void DoGripReleased(object sender, ControllerInteractionEventArgs e)
        {
            switch (CurrentHandState)
            {
                case HandState.GdNonInt:
                    ChangeState(HandState.Idle);
                    break;
                case HandState.GdInt:
                    EndInteraction(null);
                    break;
                case HandState.LaserInt:
                    if (LaserStyle == InteractionStyles.GdToInt)
                        EndInteraction(null);
                    break;
                default:
                    break;
            }

        }

        private void DoTouchpadAxisChanged(object sender, ControllerInteractionEventArgs e)
        {

        }

        private void DoApplicationMenuPressed(object sender, ControllerInteractionEventArgs e)
        {

        }


        #endregion

        #region UpdateFunctions

        private void LateUpdateLaser()
        {
            _line.material.SetColor("_Color", LineColor);
            _line.SetColors(LineColor, LineColor);
            _line.SetWidth(LineWidth, LineWidth);
            VGIS_Interactable target;
            RaycastHit hitInfo;
            bool hit = Physics.Raycast(this.transform.GetComponentInChildren<Transform>().position,
                StartPoint.transform.forward, out hitInfo, 1000);
            Vector3 endPoint;

            if (hit && hitInfo.transform.GetComponent<VGIS_Interactable>() && CurrentHandState == HandState.LaserNonInt)
            {
                target = SelectEntireObject
                    ? hitInfo.transform.GetComponent<VGIS_Interactable>()
                    : hitInfo.collider.gameObject.GetComponent<VGIS_Interactable>();
            }
            else
            {
                target = null;
            }
            UpdateHoverHightlight(target);

            if (hit == true && CurrentlyInteracting == null)
            {
                InteractionPoint.transform.rotation = this.transform.rotation;
                endPoint = hitInfo.point;
                if (CurrentHandState != HandState.LaserInt)
                {
                    InteractionPoint.transform.position = endPoint;
                }
                Hit = hitInfo;
            }
            else if (CurrentlyInteracting != null)
            {
                float value = (InteractionPoint.transform.localPosition.z + deltaPadScroll < 0)
                    ? 0
                    : InteractionPoint.transform.localPosition.z + deltaPadScroll;
                InteractionPoint.transform.localPosition = new Vector3(InteractionPoint.transform.localPosition.x,
                    InteractionPoint.transform.localPosition.y, value);
                endPoint = InteractionPoint.transform.position;
            }
            else
            {
                InteractionPoint.transform.rotation = this.transform.rotation;
                endPoint = StartPoint.transform.position + (StartPoint.transform.forward * 1000f);
                InteractionPoint.transform.position = endPoint;
                Hit = new RaycastHit();
            }
            //todo: use distance difference betweek graphics raycast and physics?
            if (ViveControllerInput2.Instance.IsCursorActive(this.GetComponent<SteamVR_TrackedObject>()) &&
                CurrentlyInteracting == null)
            {
                _line.material.SetColor("_Color", LineColorMenu);
                endPoint = ViveControllerInput2.Instance.CursorPosition(this.GetComponent<SteamVR_TrackedObject>());
                IsUsingMenu = true;
            }
            else
            {
                _line.material.SetColor("_Color", LineColor);
                IsUsingMenu = false;
            }

            _line.SetPositions(new Vector3[] { StartPoint.transform.position, endPoint });

        }

        private void LateUpdateHandHoverHightlight()
        {
            VGIS_Interactable target = null;
            float closestDistance = float.MaxValue;
            foreach (var hovering in CurrentlyHoveringOver)
            {
                if (hovering.Key == null) continue;

                float distance = Vector3.Distance(this.transform.position, hovering.Key.transform.position);
                if (!(distance < closestDistance)) continue;
                closestDistance = distance;

                target = SelectEntireObject
                    ? hovering.Key.transform.root.GetComponent<VGIS_Interactable>()
                    : hovering.Key;
            }
            UpdateHoverHightlight(target);
        }

        private void UpdateCheckSnapDistance()
        {
            VGIS_InteractableItemSnapable snappy = (VGIS_InteractableItemSnapable)CurrentlyInteracting;
            float distance = snappy.Assembly.GetDistanceToGhost(snappy);
            float cutoff = snappy.Renderer.bounds.size.magnitude * SnapDistanceMultiplier;
            if (distance > cutoff)
            {
                snappy.Assembly.SetGhostState(snappy, GhostStates.GhostNoSnap);
                foreach (var snappyCollider in snappy.Colliders)
                {
                    snappyCollider.isTrigger = false;
                }
            }
            else
            {
                snappy.Assembly.SetGhostState(snappy, GhostStates.GhostSnap);
                foreach (var snappyCollider in snappy.Colliders)
                {
                    snappyCollider.isTrigger = true;
                }
            }
        }

        #endregion

    }

    #region Enums

    /// <summary>
    /// The visibility level of the controller.
    /// </summary>
    public enum VisibilityLevel
    {
        Invisible = 0,
        Ghost = 70,
        Visible = 100,
    }

    /// <summary>
    /// The possible states of the hand.
    /// </summary>
    public enum HandState
    {
        Uninitialized,
        Idle,
        GdNonInt,
        GdInt,
        GtNonInt,
        GtInt,
        LaserNonInt,
        LaserInt,
        MenuInt
    }

    /// <summary>
    /// Determines the interaction mode of the controller based interaction.
    /// </summary>
    public enum InteractionStyles
    {
        GdToInt,
        GtToInt
    }

    #endregion
}
