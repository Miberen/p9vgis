﻿using System;
using UnityEngine;
using System.Collections;
using System.Text;
using UnityEngine.UI;
using VRTK;

public class VGIS_TextInput : MonoBehaviour
{
    public static VGIS_TextInput Instance; 
    public InputField TargetInputField;

    // Use this for initialization
    void Start ()
    {
        Instance = this;
	    SetupKeyboard();
	}
    //EventSystemManager.currentSystem
    #region Keyboard

    private void SetupKeyboard()
    {
        SteamVR_Utils.Event.Listen("KeyboardCharInput", OnKeyboard);
        SteamVR_Utils.Event.Listen("KeyboardClosed", OnKeyboardClosed);
    }

    private void OnKeyboard(object[] args)
    {
        string derp = "";
        StringBuilder stringBuilder = new StringBuilder(256);
        SteamVR.instance.overlay.GetKeyboardText(stringBuilder, 256);

        if (stringBuilder.ToString() == "\b")
        {
            // User hit backspace
            if (TargetInputField.text.Length > 0)
            {
                TargetInputField.text = TargetInputField.text.Substring(0, TargetInputField.text.Length - 1);
            }
        }
        else if (stringBuilder.ToString() == "\x1b")
        {
            // Close the keyboard
            SteamVR.instance.overlay.HideKeyboard();
        }
        else
        {
            derp = stringBuilder.ToString();

        }
        try
        {
            TargetInputField.text += derp;
        }
        catch (NullReferenceException)
        {
            //Debug.LogError("NulRef was Caught");
        }

        // Do something with the accumulated text here
    }

    private void OnKeyboardClosed(object[] args)
    {
        TargetInputField.MoveTextEnd(false);
        TargetInputField.DeactivateInputField();
        TargetInputField = null;
    }

    #endregion
}
