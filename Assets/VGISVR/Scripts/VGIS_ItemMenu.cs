﻿using UnityEngine;
using System.Collections;
using System.Globalization;
using System.Security.Policy;
using IBM.Watson.DeveloperCloud.DataTypes;
using UnityEngine.UI;
using VGIS;

public class VGIS_ItemMenu : MonoBehaviour
{
    public InputField FieldX, FieldY, FieldZ, FieldScale;
    public Slider SliderX, SliderY, SliderZ, SliderScale;

    private float _prevX, _prevY, _prevZ;
    public ColorPicker picker;
    public VGIS_Interactable item;
    public new Renderer renderer;
    public Material LineMaterial;
    private Vector3[] corners = new Vector3[4];
    private float offset;
    private bool _isInit = false;
    // Use this for initialization

    void Awake()
    {
        _isInit = false;
    }

    void Start()
    {
        picker.CurrentColor = renderer.material.color;
        picker.onValueChanged.AddListener(color =>
        {
            renderer.material.color = color;
        });

        renderer.material.color = picker.CurrentColor;
        Init();

        if (GetComponent<LineRenderer>() != null)
            GetComponent<LineRenderer>().material = LineMaterial;

        GetComponent<RectTransform>().GetWorldCorners(corners);
        offset = (corners[1].y - corners[0].y) / 2;
    }

    public void Init()
    {
        //Debug.Log(item.transform.eulerAngles);
        //Debug.Log(item.transform.eulerAngles.x.ToString(CultureInfo.InvariantCulture));
        FieldX.text = item.transform.rotation.eulerAngles.x.ToString();
        FieldY.text = item.transform.rotation.eulerAngles.y.ToString();
        FieldZ.text = item.transform.rotation.eulerAngles.z.ToString();
        FieldScale.text = item.transform.localScale.x.ToString();

        SliderX.value = item.transform.rotation.eulerAngles.x;
        SliderY.value = item.transform.rotation.eulerAngles.y;
        SliderZ.value = item.transform.rotation.eulerAngles.z;
        SliderScale.value = item.transform.localScale.x;

        _prevX = _prevY = _prevY = 0;

        _isInit = true;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.DrawLine(corners[0], VGIS_Player.Instance.Head.transform.position, Color.blue);
        //Debug.DrawLine(corners[1], VGIS_Player.Instance.Head.transform.position, Color.red);
        //Debug.DrawLine(corners[2], VGIS_Player.Instance.Head.transform.position, Color.green);
        //Debug.DrawLine(corners[3], VGIS_Player.Instance.Head.transform.position, Color.yellow);
        //Debug.DrawRay(corners[3], Vector3.down, Color.red);
        //Debug.DrawRay(corners[3], (corners[3]-VGIS_Player.Instance.Head.transform.position)*-1, Color.cyan);

        if (GetComponent<LineRenderer>() == null) return;
        RaycastHit hitdown;
        if (Physics.Raycast(this.transform.position, Vector3.down, out hitdown, 50))
        {
            if (Vector3.Distance(hitdown.point, this.transform.position) < offset)
            {
                transform.position = Vector3.Lerp(transform.position, transform.position + (Vector3.up * 2), Time.deltaTime);
                transform.LookAt(VGIS_Player.Instance.Head.transform);
            }
            else if (Vector3.Distance(hitdown.point, this.transform.position) > offset * 2)
            {
                transform.position = Vector3.Lerp(transform.position, transform.position + (Vector3.down * 2), Time.deltaTime);
                transform.LookAt(VGIS_Player.Instance.Head.transform);
            }
            GetComponent<RectTransform>().GetWorldCorners(corners);
        }

        RaycastHit hithead;
        if (Physics.Raycast(corners[3], (corners[3] - VGIS_Player.Instance.Head.transform.position) * -1, out hithead))
        {
            if (hithead.transform == item.transform)
            {
                transform.position = Vector3.Lerp(transform.position,
                    transform.position + VGIS_Player.Instance.Head.transform.right * 2, Time.deltaTime);
                transform.LookAt(VGIS_Player.Instance.Head.transform);
            }
        }

        LineRenderer line = GetComponent<LineRenderer>();
        line.SetPosition(0, corners[3]);
        line.SetPosition(1, item.transform.position);
    }

    public void OnXChanged(Slider slider)
    {
        if (!_isInit) return;
        var temp = _prevX - slider.value;
        item.transform.Rotate(temp, 0, 0);
        _prevX = slider.value;
    }
    public void OnYChanged(Slider slider)
    {
        if (!_isInit) return;
        var temp = _prevY - slider.value;
        item.transform.Rotate(0, temp, 0);
        _prevY = slider.value;
    }
    public void OnZChanged(Slider slider)
    {
        if (!_isInit) return;
        var temp = _prevZ - slider.value;
        item.transform.Rotate(0, 0, temp);
        _prevZ = slider.value;
    }
    public void OnScaleChanged(Slider slider)
    {
        item.transform.localScale = new Vector3(slider.value, slider.value, slider.value);
    }

}
