﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;
using JetBrains.Annotations;
using NewtonVR;
using NewtonVR.Example;
using Valve.VR;

public class EasyController : MonoBehaviour
{
    private SteamVR_RenderModel rModel;
    private VRController_Extended ectrler;
    private Transform target;
    private bool isGrabbing = false;
    public bool laserIsToggle = true;
    public bool touchPadIsWheel = false;
    public float previousPadValue = 0;
    public float deltaPadScroll = 0;
    public float scrollSensitivity = 1;
    public Vector2 point = new Vector2();
    private InteractionStyle orgStyle;
    private NVRExampleLaserPointer laserInteract;
    // Use this for initialization
    void Start()
    {
        laserInteract = transform.GetComponent<NVRExampleLaserPointer>();
        ectrler = GetComponent<VRController_Extended>();
        rModel = transform.GetComponentInChildren<SteamVR_RenderModel>();

        ectrler.TriggerPressed += TriggerPressed;
        ectrler.PadPressed += PadPressed;
        ectrler.PadPressedUp += PadPressedUp;
        ectrler.PadPressedDown += PadPressedDown;
        ectrler.PadPressedLeft += PadPressedLeft;
        ectrler.PadPressedRight += PadPressedRight;
        ectrler.MenuPressed += MenuPressed;
        ectrler.MenuUnpressed += MenuUnpressed;
        ectrler.PadTouched += PadTouched;
        ectrler.PadUntouched += PadUntouched;
        //rModel.controllerModeState.bScrollWheelVisible = true;
    }

    // Update is called once per frame
    void Update()
    {
        //Handle scrolling
        if (touchPadIsWheel && ectrler.padTouched && previousPadValue != 0f)
        {
            deltaPadScroll = ((ectrler.controllerState.rAxis0.y + 1 - previousPadValue + 1) - 2) * scrollSensitivity * Mathf.Min(Vector3.Distance(laserInteract.StartPoint.transform.localPosition, laserInteract.InteractionPoint.transform.localPosition), 3f);
        }
        previousPadValue = ectrler.controllerState.rAxis0.y;
    }

    void TriggerPressed(object sender, ClickedEventArgs e)
    {
        if (target != null)
        {
            if (isGrabbing)
            {

                target.transform.parent = null;
                isGrabbing = !isGrabbing;

            }
            else
            {
                target.SetParent(this.transform);
                isGrabbing = !isGrabbing;
            }
        }
    }

    void PadPressed(object sender, ClickedEventArgs e)
    {
        point = new Vector2(e.padX, e.padY);

    }
    void PadPressedUp(object sender, ClickedEventArgs e)
    {
        point = new Vector2(e.padX, e.padY);
    }
    void PadPressedDown(object sender, ClickedEventArgs e)
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.position = transform.position;
        cube.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    }
    void PadPressedLeft(object sender, ClickedEventArgs e)
    {
        point = new Vector2(e.padX, e.padY);
    }
    void PadPressedRight(object sender, ClickedEventArgs e)
    {
        point = new Vector2(e.padX, e.padY);
    }

    void MenuPressed(object sender, ClickedEventArgs e)
    {
        transform.GetComponent<NVRExampleLaserPointer>().LaserVisible = !transform.GetComponent<NVRExampleLaserPointer>().LaserVisible;
        NVRHand hand = transform.GetComponent<NVRHand>();

        if (laserIsToggle)
        {
            if (hand.CurrentHandState == HandState.Idle)
            {
                hand.CurrentHandState = HandState.LaserOnNotInteracting;
                orgStyle = hand.CurrentInteractionStyle;
                hand.CurrentInteractionStyle = InteractionStyle.GripToggleToInteract;
            }
            else
            {
                hand.CurrentHandState = HandState.Idle;
                hand.CurrentInteractionStyle = orgStyle;
            }
        }
        else
        {
            if (hand.CurrentHandState == HandState.Idle)
            {
                hand.CurrentHandState = HandState.LaserOnNotInteracting;
                orgStyle = hand.CurrentInteractionStyle;
                hand.CurrentInteractionStyle = InteractionStyle.GripDownToInteract;
            }
            else
            {
                hand.CurrentHandState = HandState.Idle;
                hand.CurrentInteractionStyle = orgStyle;
            }
        }

        //if (hand.CurrentHandState == HandState.Idle)
        //{
        //    hand.CurrentHandState = HandState.LaserOnNotInteracting;
        //}
        //else
        //{
        //    transform.GetComponent<NVRHand>().CurrentHandState = HandState.Idle;
        //}
    }

    void MenuUnpressed(object sender, ClickedEventArgs e)
    {

    }

    void PadTouched(object sender, ClickedEventArgs e)
    {

    }

    void PadUntouched(object sender, ClickedEventArgs e)
    {
        deltaPadScroll = 0;
    }
}
