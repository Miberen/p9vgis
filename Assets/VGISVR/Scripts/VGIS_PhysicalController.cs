﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NewtonVR;

namespace VGIS
{
    public class VGIS_PhysicalController : MonoBehaviour
    {
        private VGIS_Interaction Hand;
        public bool State = false;
        private Rigidbody Rigidbody;

        private Collider[] Colliders;
        private GameObject PhysicalController;
        private Transform ModelParent;

        protected float DropDistance { get { return 1f; } }
        protected Vector3 ClosestHeldPoint;

        protected float AttachedRotationMagic = 20f;
        protected float AttachedPositionMagic = 3000f;

        public void Initialize(VGIS_Interaction trackingHand, bool initialState)
        {
            Hand = trackingHand;

            PhysicalController = GameObject.Instantiate(Hand.gameObject);
            PhysicalController.name = PhysicalController.name.Replace("(Clone)", " [Physical]");

            SteamVR_RenderModel renderModel = PhysicalController.GetComponentInChildren<SteamVR_RenderModel>();
            ModelParent = renderModel.transform;

            foreach (var p in PhysicalController.gameObject.GetComponents<Component>())
            {
                if (!(p is Rigidbody || p is Transform))
                {                    
                    DestroyImmediate(p);
                }
            }

            foreach (Transform t in PhysicalController.transform)
            {
                if (t.ToString().Contains("Canvas"))
                {
                    Destroy(t.gameObject);
                }                 
            }           
           

            Collider[] clonedColliders = PhysicalController.GetComponentsInChildren<Collider>();
            for (int index = 0; index < clonedColliders.Length; index++)
            {
                GameObject.DestroyImmediate(clonedColliders[index]);
            }

            PhysicalController.transform.parent = Hand.transform.parent;
            PhysicalController.transform.position = Hand.transform.position;
            PhysicalController.transform.rotation = Hand.transform.rotation;
            PhysicalController.transform.localScale = Hand.transform.localScale;

            string controllerModel = Hand.GetDeviceName();
            switch (controllerModel)
            {
                case "vr_controller_05_wireless_b":
                    Transform dk1Trackhat = ModelParent.transform.Find("trackhat");
                    Collider dk1TrackhatCollider = dk1Trackhat.gameObject.GetComponent<BoxCollider>();
                    if (dk1TrackhatCollider == null)
                        dk1TrackhatCollider = dk1Trackhat.gameObject.AddComponent<BoxCollider>();

                    Transform dk1Body = ModelParent.transform.Find("body");
                    Collider dk1BodyCollider = dk1Body.gameObject.GetComponent<BoxCollider>();
                    if (dk1BodyCollider == null)
                        dk1BodyCollider = dk1Body.gameObject.AddComponent<BoxCollider>();

                    Colliders = new Collider[] { dk1TrackhatCollider, dk1BodyCollider };
                    break;

                case "vr_controller_vive_1_5":
                    Transform dk2TrackhatColliders = ModelParent.transform.FindChild("VivePreColliders");
                    if (dk2TrackhatColliders == null)
                    {
                        dk2TrackhatColliders = GameObject.Instantiate(Resources.Load<GameObject>("VivePreColliders")).transform;
                        dk2TrackhatColliders.parent = ModelParent.transform;
                        dk2TrackhatColliders.localPosition = Vector3.zero;
                        dk2TrackhatColliders.localRotation = Quaternion.identity;
                        dk2TrackhatColliders.localScale = Vector3.one;
                    }

                    Colliders = dk2TrackhatColliders.GetComponentsInChildren<Collider>();
                    break;

                case "Custom":
                    Transform customCollidersTransform = PhysicalController.transform.FindChild("VivePreColliders");
                    if (customCollidersTransform == null)
                    {
                        GameObject customColliders = GameObject.Instantiate(Hand.CustomPhysicalColliders);
                        customColliders.name = "CustomColliders";
                        customCollidersTransform = customColliders.transform;

                        customCollidersTransform.parent = PhysicalController.transform;
                        customCollidersTransform.localPosition = Vector3.zero;
                        customCollidersTransform.localRotation = Quaternion.identity;
                        customCollidersTransform.localScale = Vector3.one;
                    }

                    Colliders = customCollidersTransform.GetComponentsInChildren<Collider>();
                    break;

                default:
                    Debug.LogError("Error. Unsupported device type: " + controllerModel);
                    break;
            }

            Rigidbody = PhysicalController.GetComponent<Rigidbody>();
            Rigidbody.isKinematic = false;
            Rigidbody.maxAngularVelocity = float.MaxValue;

            Renderer[] renderers = PhysicalController.GetComponentsInChildren<Renderer>();
            for (int index = 0; index < renderers.Length; index++)
            {
                NVRHelpers.SetOpaque(renderers[index].material);
            }

            if (initialState == false)
            {
                Off();
            }
            else
            {
                On();
            }
        }

        public void Kill()
        {
            Destroy(PhysicalController);
            Destroy(this);
        }

        private void CheckForDrop()
        {
            float distance = Vector3.Distance(Hand.transform.position, this.transform.position);

            if (distance > DropDistance)
            {
                DroppedBecauseOfDistance();
            }
        }

        private void UpdatePosition()
        {
            Rigidbody.maxAngularVelocity = float.MaxValue; //this doesn't seem to be respected in nvrhand's init. or physical hand's init. not sure why. if anybody knows, let us know. -Keith 6/16/2016

            float angle;
            Vector3 axis;

            var rotationDelta = Hand.transform.rotation * Quaternion.Inverse(PhysicalController.transform.rotation);
            var positionDelta = (Hand.transform.position - PhysicalController.transform.position);

            rotationDelta.ToAngleAxis(out angle, out axis);

            if (angle > 180)
                angle -= 360;

            if (angle != 0)
            {
                Vector3 angularTarget = angle * axis;
                this.Rigidbody.angularVelocity = angularTarget;
            }

            Vector3 velocityTarget = positionDelta / Time.fixedDeltaTime;
            this.Rigidbody.velocity = velocityTarget;
        }

        protected virtual void FixedUpdate()
        {
            if (State == true)
            {
                CheckForDrop();

                UpdatePosition();
            }
        }

        protected virtual void DroppedBecauseOfDistance()
        {
            Hand.ForceGhost();
        }

        public void On()
        {
            PhysicalController.transform.position = Hand.transform.position;
            PhysicalController.transform.rotation = Hand.transform.rotation;

            PhysicalController.SetActive(true);

            State = true;
        }

        public void Off()
        {
            PhysicalController.SetActive(false);

            State = false;
        }
    }
}