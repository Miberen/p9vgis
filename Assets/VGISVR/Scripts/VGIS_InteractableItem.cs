﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using VGIS;

namespace VGIS
{

    public class VGIS_InteractableItem : VGIS_Interactable
    {
        [Tooltip("If you have a specific point you'd like the object held at, create a transform there and set it to this variable")]
        public Transform InteractionPoint;

        protected Transform PickupTransform;
        protected GameObject Menu;
        protected GameObject Handle;
        public Color HightlightColor;

        protected override void Awake()
        {
            base.Awake();
            if(transform.GetComponent<Rigidbody>() != null)
                this.Rigidbody.maxAngularVelocity = 100f;
        }

        public override void OnNewPosesApplied()
        {
            base.OnNewPosesApplied();

            if (IsAttached == true)
            {

                Quaternion RotationDelta;
                Vector3 PositionDelta;

                float angle;
                Vector3 axis;
             
                if (InteractionPoint != null)
                {
                    if (AttachedHand.CurrentHandState == HandState.LaserInt)
                    {

                        RotationDelta = AttachedHand.InteractionPoint.transform.rotation * Quaternion.Inverse(InteractionPoint.rotation);
                        PositionDelta = AttachedHand.InteractionPoint.transform.position - InteractionPoint.position;
                    }
                    else
                    {
                        RotationDelta = AttachedHand.transform.rotation * Quaternion.Inverse(InteractionPoint.rotation);
                        PositionDelta = (AttachedHand.transform.position - InteractionPoint.position);
                    }
                }
                else
                {
                    RotationDelta = PickupTransform.rotation * Quaternion.Inverse(this.transform.rotation);
                    PositionDelta = (PickupTransform.position - this.transform.position);
                }

                RotationDelta.ToAngleAxis(out angle, out axis);

                if (angle > 180)
                    angle -= 360;

                if (angle != 0)
                {
                    Vector3 AngularTarget = angle * axis;
                    this.Rigidbody.angularVelocity = Vector3.MoveTowards(this.Rigidbody.angularVelocity, AngularTarget, 10f * (deltaPoses * 1000));
                }

                Vector3 VelocityTarget = PositionDelta / deltaPoses;
                this.Rigidbody.velocity = Vector3.MoveTowards(this.Rigidbody.velocity, VelocityTarget, 10f);
            }
        }

        public override void BeginInteraction(VGIS_Interaction hand)
        {
            base.BeginInteraction(hand);

            if (hand.CurrentHandState == HandState.LaserInt)
            {
                PickupTransform = new GameObject(string.Format("[{0}] VGISPickupTransform", this.gameObject.name)).transform;
                PickupTransform.parent = hand.InteractionPoint.transform;
                PickupTransform.position = this.transform.position;
                PickupTransform.rotation = this.transform.rotation;
            }
            else
            {
                PickupTransform = new GameObject(string.Format("[{0}] VGISPickupTransform", this.gameObject.name)).transform;
                PickupTransform.parent = hand.transform;
                PickupTransform.position = this.transform.position;
                PickupTransform.rotation = this.transform.rotation;
            }

        }

        public override void EndInteraction()
        {
            base.EndInteraction();

            if (PickupTransform != null)
                Destroy(PickupTransform.gameObject);
        }

        public override void Hightlight(Color color)
        {
            HightlightColor = color;
            if (this.transform.root.GetComponent<VGIS_SnapManager>())
            {
                foreach (KeyValuePair<GameObject, bool> part in this.GetComponent<VGIS_SnapManager>().Parts)
                {
                    if (part.Value == false) continue;
                    if (part.Key.GetComponent<VGIS_InteractableItemSnapable>().IsSelected) return;
                    List<Material> mats = new List<Material>();
                    mats.AddRange(part.Key.GetComponent<VGIS_InteractableItemSnapable>().Renderer.materials);
                    mats.Add(Instantiate((Material)Resources.Load("OutlineBasic")));
                    mats.Last().color = color;
                    part.Key.GetComponent<VGIS_InteractableItemSnapable>().Renderer.materials = mats.ToArray();
                }
            }
            else
            {
                List<Material> mats = new List<Material>();
                mats.AddRange(GetComponent<MeshRenderer>()
                    ? GetComponent<MeshRenderer>().materials
                    : GetComponentInChildren<MeshRenderer>().materials);
                mats.Add(Instantiate((Material)Resources.Load("OutlineBasic")));
                mats.Last().color = color;

                GetComponent<MeshRenderer>().materials = mats.ToArray();
            }
        }

        public override void UnHightlight()
        {
            HightlightColor = new Color();
            if (this.GetComponent<VGIS_SnapManager>())
            {
                foreach (KeyValuePair<GameObject, bool> part in this.GetComponent<VGIS_SnapManager>().Parts)
                {
                    if (part.Value == false) continue;
                    if (part.Key.GetComponent<VGIS_InteractableItemSnapable>().IsSelected) return;
                    List<Material> mats = new List<Material>();
                    mats.AddRange(part.Key.GetComponent<VGIS_InteractableItemSnapable>().Renderer.materials);
                    mats.Remove(mats.Last());

                    part.Key.GetComponent<VGIS_InteractableItemSnapable>().Renderer.materials = mats.ToArray();
                }
            }
            else
            {
                List<Material> mats = new List<Material>();
                mats.AddRange(GetComponent<MeshRenderer>()
                    ? GetComponent<MeshRenderer>().materials
                    : GetComponentInChildren<MeshRenderer>().materials);

                mats.Remove(mats.Last());

                GetComponentInChildren<MeshRenderer>().materials = mats.ToArray();
            }
        }

        public override void StartHoverHightlight(VGIS_Interaction hand, Color color)
        {
            if (IsHoverHightlighted) return;
            HoveringHand = hand;
            if (this.GetComponent<VGIS_SnapManager>())
            {              
                foreach (KeyValuePair<GameObject, bool> part in this.GetComponent<VGIS_SnapManager>().Parts)
                {
                    if (part.Value == false) continue;
                    if(part.Key.GetComponent<VGIS_Interactable>().IsHoverHightlighted) continue;
                    part.Key.GetComponent<VGIS_Interactable>().IsHoverHightlighted = true;
                    part.Key.GetComponent<VGIS_Interactable>().HoveringHand = hand;
                    _listOfHoverMaterials.AddRange(part.Key.GetComponent<VGIS_InteractableItemSnapable>().Renderer.materials);
                    foreach (Material hoverMaterial in _listOfHoverMaterials)
                    {
                        hoverMaterial.EnableKeyword("_EMISSION");
                    }
                }
            }
            else
            {
                if (IsHoverHightlighted) return;
                if (GetComponent<MeshRenderer>()) _listOfHoverMaterials.AddRange(GetComponent<MeshRenderer>().materials);
                _listOfHoverMaterials.AddRange(GetComponentInChildren<MeshRenderer>().materials);
                foreach (Material hoverMaterial in _listOfHoverMaterials)
                {
                    hoverMaterial.EnableKeyword("_EMISSION");
                }                            
            }
            HighligtherEnumerator = HoverHightlighter(_listOfHoverMaterials, color);
            IsHoverHightlighted = true;
            StartCoroutine(HighligtherEnumerator);
            CreateHandle();
        }
        
        public override void StopHoverHightlight(VGIS_Interaction hand)
        {
            if (!IsHoverHightlighted) return;
            if (HoveringHand != hand) return;

            if (this.GetComponent<VGIS_SnapManager>())
            {
                foreach (KeyValuePair<GameObject, bool> part in this.GetComponent<VGIS_SnapManager>().Parts)
                {
                    if (part.Value == false) continue;
                    if (!part.Key.GetComponent<VGIS_Interactable>().IsHoverHightlighted) continue;
                    if (part.Key.GetComponent<VGIS_Interactable>().HoveringHand != hand) continue;
                    part.Key.GetComponent<VGIS_Interactable>().IsHoverHightlighted = false;
                    part.Key.GetComponent<VGIS_Interactable>().HoveringHand = null;
                    foreach (Material hoverMaterial in _listOfHoverMaterials)
                    {
                        hoverMaterial.DisableKeyword("_EMISSION");
                    }
                }
            }
            else
            {
                if (!IsHoverHightlighted) return;
                foreach (Material hoverMaterial in _listOfHoverMaterials)
                {
                    hoverMaterial.DisableKeyword("_EMISSION");
                }               
            }
            IsHoverHightlighted = false;
            HoveringHand = null;
            StopCoroutine(HighligtherEnumerator);
            _listOfHoverMaterials.Clear();
            RemoveHandle();
        }

        public override void BeginSelect(VGIS_Interaction hand, Color color)
        {           
            IsSelected = true;
            Hightlight(color);
            Selectinghand = hand;
            
            float scaleFactor = Vector3.Distance(this.transform.position,
                                    VGIS_Player.Instance.transform.position)/3;
            if (!this.GetComponent<VGIS_SnapManager>())
            {
                GetComponent<Rigidbody>().useGravity = false;
                //Create menu
                Menu = Instantiate(Resources.Load("ItemMenu") as GameObject);
                Menu.GetComponent<VGIS_ItemMenu>().item = this;
                Menu.GetComponent<VGIS_ItemMenu>().renderer = GetComponent<MeshRenderer>();
                Menu.GetComponent<Canvas>().worldCamera = ViveControllerInput2.Instance.ControllerCamera;
                Menu.transform.localScale *= scaleFactor;
                Vector3[] corners = new Vector3[4];
                Menu.GetComponent<RectTransform>().GetWorldCorners(corners);
                float offset = (corners[1].y - corners[0].y)/2;
                Menu.transform.position = transform.position + Vector3.up*offset +
                                          VGIS_Player.Instance.Head.transform.right*
                                          GetComponent<MeshRenderer>().bounds.extents.x +
                                          VGIS_Player.Instance.Head.transform.right*offset;
                Menu.transform.LookAt(VGIS_Player.Instance.Head.transform);
                LineRenderer line = Menu.AddComponent<LineRenderer>();

                line.SetPosition(0, corners[3]);
                line.SetPosition(1, this.transform.position);
                line.SetWidth(0.05f, 0.05f);
              
            }         
        }

        public override void EndSelect()
        {
            IsSelected = false;
            GetComponent<Rigidbody>().useGravity = true;
            Selectinghand = null;
            UnHightlight();
            Destroy(Menu);
            RemoveHandle();
        }

        protected virtual void CreateHandle()
        {
            if (IsSelected) return;
            if (GetComponent<VGIS_SnapManager>())
            {
                Handle = Instantiate(Resources.Load("3DHandle") as GameObject);
                Handle.transform.localScale = Vector3.zero;
                Handle.transform.position = transform.position;
                Handle.transform.forward = this.transform.forward;
                Handle.transform.SetParent(transform);
                Handle.transform.localEulerAngles = Vector3.zero;
            }
            else
            {
                Handle = Instantiate(Resources.Load("3DHandle") as GameObject);
                Handle.transform.localScale = Vector3.zero;
                Handle.transform.position = this.GetComponent<Renderer>().bounds.center;
                Handle.transform.forward = this.transform.forward;
                Handle.transform.SetParent(transform);
                Handle.transform.localEulerAngles = Vector3.zero;
            }
            
        }

        protected virtual void RemoveHandle()
        {
            if (IsSelected || IsHoverHightlighted) return;
            Destroy(Handle);
        }
    }
}