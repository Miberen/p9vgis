﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Valve.VR;
using VGIS;
using VRTK;

public class ViveControllerInput2 : BaseInputModule
{
    public static ViveControllerInput2 Instance;

    [Header(" [Cursor setup]")]
    public Sprite CursorSprite;
    public Material CursorMaterial;
    public float NormalCursorScale = 0.00025f;

    [Space(10)]

    [Header(" [Runtime variables]")]
    [Tooltip("Indicates whether or not the gui was hit by any controller this frame")]
    public bool GuiHit;

    [Tooltip("Indicates whether or not a button was used this frame")]
    public bool ButtonUsed;

    [Tooltip("Generated cursors")]
    private RectTransform[] _cursors;

    private GameObject[] _currentPoint;
    private GameObject[] _currentPressed;
    private GameObject[] _currentDragging;

    private PointerEventData[] PointEvents;

    private bool Initialized = false;

    [Tooltip("Generated non rendering camera (used for raycasting ui)")]
    public Camera ControllerCamera;

    private SteamVR_ControllerManager ControllerManager;
    private SteamVR_TrackedObject[] Controllers;
    private SteamVR_Controller.Device[] ControllerDevices;

    public Vector3 CursorPosition(SteamVR_TrackedObject obj)
    {
        if (Controllers.ToList().Contains(obj))
        {
            if(_cursors[Controllers.ToList().IndexOf(obj)].gameObject.activeInHierarchy)
                return _cursors[Controllers.ToList().IndexOf(obj)].transform.position;
        }
        return new Vector3();
    }

    public bool IsCursorActive(SteamVR_TrackedObject obj)
    {
        if (_cursors[Controllers.ToList().IndexOf(obj)].gameObject.activeInHierarchy)
            return true;
        return false;
    }

    protected override void Start()
    {
        base.Start();

        if (Initialized == false)
        {
            Instance = this;

            ControllerCamera = new GameObject("Controller UI Camera").AddComponent<Camera>();
            ControllerCamera.clearFlags = CameraClearFlags.Nothing; //CameraClearFlags.Depth;
            ControllerCamera.cullingMask = 0; // 1 << LayerMask.NameToLayer("UI"); 
            ControllerCamera.stereoTargetEye = StereoTargetEyeMask.None;
            ControllerCamera.targetDisplay = 2;
            ControllerCamera.nearClipPlane = 0.01f;

            ControllerManager = GameObject.FindObjectOfType<SteamVR_ControllerManager>();
            Controllers = new SteamVR_TrackedObject[] { ControllerManager.left.GetComponent<SteamVR_TrackedObject>(), ControllerManager.right.GetComponent<SteamVR_TrackedObject>() };
            ControllerDevices = new SteamVR_Controller.Device[Controllers.Length];
            _cursors = new RectTransform[Controllers.Length];

            for (int index = 0; index < _cursors.Length; index++)
            {
                GameObject cursor = new GameObject("Cursor " + index);
                Canvas canvas = cursor.AddComponent<Canvas>();
                cursor.AddComponent<CanvasRenderer>();
                cursor.AddComponent<CanvasScaler>();
                cursor.AddComponent<UIIgnoreRaycast>();
                cursor.AddComponent<GraphicRaycaster>();

                canvas.renderMode = RenderMode.WorldSpace;
                canvas.sortingOrder = 1000; //set to be on top of everything

                Image image = cursor.AddComponent<Image>();
                image.sprite = CursorSprite;
                image.material = CursorMaterial;


                if (CursorSprite == null)
                    Debug.LogError("Set CursorSprite on " + this.gameObject.name + " to the sprite you want to use as your cursor.", this.gameObject);

                _cursors[index] = cursor.GetComponent<RectTransform>();
            }

            _currentPoint = new GameObject[_cursors.Length];
            _currentPressed = new GameObject[_cursors.Length];
            _currentDragging = new GameObject[_cursors.Length];
            PointEvents = new PointerEventData[_cursors.Length];

            Canvas[] canvases = GameObject.FindObjectsOfType<Canvas>();
            foreach (Canvas canvas in canvases)
            {
                canvas.worldCamera = ControllerCamera;
            }

            Initialized = true;
        }
    }

    // use screen midpoint as locked pointer location, enabling look location to be the "mouse"
    private bool GetLookPointerEventData(int index)
    {
        if (PointEvents[index] == null)
            PointEvents[index] = new PointerEventData(base.eventSystem);
        else
            PointEvents[index].Reset();

        PointEvents[index].delta = Vector2.zero;
        PointEvents[index].position = new Vector2(Screen.width / 2, Screen.height / 2);
        PointEvents[index].scrollDelta = Vector2.zero;

        base.eventSystem.RaycastAll(PointEvents[index], m_RaycastResultCache);
        PointEvents[index].pointerCurrentRaycast = FindFirstRaycast(m_RaycastResultCache);
        if (PointEvents[index].pointerCurrentRaycast.gameObject != null)
        {
            GuiHit = true; //gets set to false at the beginning of the process event
        }

        m_RaycastResultCache.Clear();

        return true;
    }

    // update the cursor location and whether it is enabled
    // this code is based on Unity's DragMe.cs code provided in the UI drag and drop example
    private void UpdateCursor(int index, PointerEventData pointData)
    {
        if (PointEvents[index].pointerCurrentRaycast.gameObject != null && !VGIS_Player.Instance.Hands[index].CurrentlyInteracting && !VGIS_Player.Instance.Hands[index].IsHovering)
        {
            _cursors[index].gameObject.SetActive(true);

            if (pointData.pointerEnter != null)
            {
                RectTransform draggingPlane = pointData.pointerEnter.GetComponent<RectTransform>();
                Vector3 globalLookPos;
                if (RectTransformUtility.ScreenPointToWorldPointInRectangle(draggingPlane, pointData.position, pointData.enterEventCamera, out globalLookPos))
                {
                    _cursors[index].position = globalLookPos;
                    _cursors[index].rotation = draggingPlane.rotation;

                    // scale cursor based on distance to camera
                    float lookPointDistance = (_cursors[index].position - Camera.main.transform.position).magnitude;
                    float cursorScale = lookPointDistance * NormalCursorScale;
                    if (cursorScale < NormalCursorScale)
                    {
                        cursorScale = NormalCursorScale;
                    }

                    _cursors[index].localScale = Vector3.one * cursorScale;
                }
            }
        }
        else
        {
            _cursors[index].gameObject.SetActive(false);
        }
    }

    // clear the current selection
    public void ClearSelection()
    {
        if (base.eventSystem.currentSelectedGameObject)
        {
            base.eventSystem.SetSelectedGameObject(null);
        }
    }

    // select a game object
    private void Select(GameObject go)
    {
        ClearSelection();

        if (ExecuteEvents.GetEventHandler<ISelectHandler>(go))
        {
            base.eventSystem.SetSelectedGameObject(go);
        }
    }

    // send update event to selected object
    // needed for InputField to receive keyboard input
    private bool SendUpdateEventToSelectedObject()
    {
        if (base.eventSystem.currentSelectedGameObject == null)
            return false;

        BaseEventData data = GetBaseEventData();

        ExecuteEvents.Execute(base.eventSystem.currentSelectedGameObject, data, ExecuteEvents.updateSelectedHandler);

        return data.used;
    }

    private void UpdateCameraPosition(int index)
    {
        if (!Controllers[index].GetComponent<VGIS_Interaction>().StartPoint) return;
        ControllerCamera.transform.position = Controllers[index].GetComponent<VGIS_Interaction>().StartPoint.transform.position;
        ControllerCamera.transform.forward = Controllers[index].GetComponent<VGIS_Interaction>().StartPoint.transform.forward;
    }

    private void InitializeControllers()
    {
        for (int index = 0; index < Controllers.Length; index++)
        {
            if (Controllers[index] != null && Controllers[index].index != SteamVR_TrackedObject.EIndex.None)
            {
                ControllerDevices[index] = SteamVR_Controller.Input((int)Controllers[index].index);
            }
            else
            {
                ControllerDevices[index] = null;
            }
        }
    }

    // Process is called by UI system to process events
    public override void Process()
    {      
        InitializeControllers();

        GuiHit = false;
        ButtonUsed = false;

        // send update events if there is a selected object - this is important for InputField to receive keyboard events
        SendUpdateEventToSelectedObject();
     
        // see if there is a UI element that is currently being looked at
        for (int index = 0; index < _cursors.Length; index++)
        {
            if (Controllers[index].gameObject.activeInHierarchy == false)
            {
                if (_cursors[index].gameObject.activeInHierarchy == true)
                {
                    _cursors[index].gameObject.SetActive(false);
                }
                continue;
            }

            UpdateCameraPosition(index);

            bool hit = GetLookPointerEventData(index);
            if (hit == false)
                continue;

            _currentPoint[index] = PointEvents[index].pointerCurrentRaycast.gameObject;

            // handle enter and exit events (highlight)
            base.HandlePointerExitAndEnter(PointEvents[index], _currentPoint[index]);

            // update cursor
            UpdateCursor(index, PointEvents[index]);

            if (Controllers[index] != null && !VGIS_Player.Instance.Hands[index].CurrentlyInteracting && !VGIS_Player.Instance.Hands[index].IsHovering)
            {
                if (ButtonDown(index))
                {
                    ClearSelection();

                    PointEvents[index].pressPosition = PointEvents[index].position;
                    PointEvents[index].pointerPressRaycast = PointEvents[index].pointerCurrentRaycast;
                    PointEvents[index].pointerPress = null;

                    if (_currentPoint[index] != null)
                    {
                        _currentPressed[index] = _currentPoint[index];

                        GameObject newPressed = ExecuteEvents.ExecuteHierarchy(_currentPressed[index], PointEvents[index], ExecuteEvents.pointerDownHandler);

                        if (newPressed == null)
                        {
                            // some UI elements might only have click handler and not pointer down handler
                            newPressed = ExecuteEvents.ExecuteHierarchy(_currentPressed[index], PointEvents[index], ExecuteEvents.pointerClickHandler);
                            if (newPressed != null)
                            {
                                _currentPressed[index] = newPressed;
                            }
                        }
                        else
                        {
                            _currentPressed[index] = newPressed;
                            // we want to do click on button down at same time, unlike regular mouse processing
                            // which does click when mouse goes up over same object it went down on
                            // reason to do this is head tracking might be jittery and this makes it easier to click buttons
                            ExecuteEvents.Execute(newPressed, PointEvents[index], ExecuteEvents.pointerClickHandler);
                        }

                        if (newPressed != null)
                        {
                            PointEvents[index].pointerPress = newPressed;
                            _currentPressed[index] = newPressed;
                            Select(_currentPressed[index]);
                            ButtonUsed = true;
                        }

                        ExecuteEvents.Execute(_currentPressed[index], PointEvents[index], ExecuteEvents.beginDragHandler);
                        PointEvents[index].pointerDrag = _currentPressed[index];
                        _currentDragging[index] = _currentPressed[index];
                    }
                }
                
                if (ButtonUp(index))
                {
                    if (_currentDragging[index])
                    {
                        ExecuteEvents.Execute(_currentDragging[index], PointEvents[index], ExecuteEvents.endDragHandler);
                        if (_currentPoint[index] != null)
                        {
                            ExecuteEvents.ExecuteHierarchy(_currentPoint[index], PointEvents[index], ExecuteEvents.dropHandler);
                        }
                        PointEvents[index].pointerDrag = null;
                        _currentDragging[index] = null;
                    }
                    if (_currentPressed[index])
                    {
                        ExecuteEvents.Execute(_currentPressed[index], PointEvents[index], ExecuteEvents.pointerUpHandler);
                        PointEvents[index].rawPointerPress = null;
                        PointEvents[index].pointerPress = null;
                        _currentPressed[index] = null;
                    }
                }

                // drag handling
                if (_currentDragging[index] != null)
                {
                    ExecuteEvents.Execute(_currentDragging[index], PointEvents[index], ExecuteEvents.dragHandler);
                }
            }
        }
    }

    private bool ButtonDown(int index)
    {
        ControllerDevices[index].hairTriggerDelta = 1f; //hack
        return (ControllerDevices[index] != null && ControllerDevices[index].GetHairTriggerDown());
        //return (ControllerDevices[index] != null && ControllerDevices[index].GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger) == true);
    }

    private bool ButtonUp(int index)
    {
        ControllerDevices[index].hairTriggerDelta = 0.1f; //hack
        return (ControllerDevices[index] != null && ControllerDevices[index].GetHairTriggerUp());
        //return (ControllerDevices[index] != null && ControllerDevices[index].GetPressUp(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger) == true);
    }
}